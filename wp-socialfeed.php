<?php
/**
 * Plugin Name: WP Social Feed
 * Plugin URI: https://gitlab.com/gaambo/wp-social-feed
 * Description: a simple basic implementation for fetching social feeds
 * Version: 0.1.0
 * Author: gaambo
 * Author URI: http://gambo.at
 *
 * Text Domain: wpsf
 * Domain Path: /languages/
 *
 * @package WP_Social_Feed
 * @category Core
 * @author gaambo
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function WP_Social_Feed() {
	return WP_Social_Feed::instance();
}

add_action( 'init', 'WP_Social_Feed' );

require_once( 'vendor/autoload.php' ); 

require_once( 'includes/helper/helper.php' ); 
require_once( 'includes/class-wpsf-settings.php' );
require_once( 'includes/class-wpsf-feed-abstract.php' );
require_once( 'includes/class-wpsf-feed.php' );
require_once( 'includes/class-wpsf-shortcodes.php' );
require_once( 'includes/class-wpsf-enqueues.php' );

//default feeds
require_once( 'includes/class-wpsf-feed-facebook.php' ); 
require_once( 'includes/helper/facebook-helper.php' ); 
require_once( 'includes/class-wpsf-feed-instagram.php' ); 
require_once( 'includes/helper/instagram-helper.php' ); 



if( is_admin() ) {
    require_once( 'includes/class-wpsf-admin.php' );
}

if( !class_exists( 'WP_Social_Feed' ) ) {

    final class WP_Social_Feed {

        private static $_instance = null;

        public $name;

        public $version;

        public $plugin_url;

        public $plugin_path;

        public $admin;

        public $settings;

        // registerd social networks
        public $social_feeds; 

        //current feed objects for public global access 
        public $feeds;


        public function __construct () {
            $this->name 			= 'wp-socialfeed';
            $this->plugin_url 		= plugin_dir_url( __FILE__ );
            $this->plugin_path 		= plugin_dir_path( __FILE__ );
            $this->version 			= '0.1.0';

            // Admin - End
            register_activation_hook( __FILE__, array( $this, 'install' ) );
            add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

            //init settings
            $this->settings = WP_Social_Feed_Settings::instance();
            WP_Social_Feed_Shortcodes::instance(); 

            //init admin
            if ( is_admin() ) {
                $this->admin = WP_Social_Feed_Admin::instance();
            }

            //init shortcode
            WP_Social_Feed_Shortcodes::instance(); 

            //init enqueues
            WP_Social_Feed_Enqueues::instance(); 

            //feeds
            $social_feeds = array(); 
            $this->social_feeds = apply_filters( 'wpsf/feeds', $social_feeds ); 

            //template directory
            define( 'TEMPLATE_THEME_DIR', apply_filters( 'wpsf/template_theme_dir', 'wpsf-templates' ) ); 

        }

        public static function instance () {
            if ( is_null( self::$_instance ) )
                self::$_instance = new self();
            return self::$_instance;
        }

        public function load_plugin_textdomain() {
            load_plugin_textdomain( 'wp-socialfeed', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        }

        public function __clone () {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
        }

        public function __wakeup () {
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), '1.0.0' );
        }

        public function install () {
            $this->_log_version_number();
        }

        private function _log_version_number () {
            // Log the version number.
            update_option( $this->token . '-version', $this->version );
        }

    }
}

