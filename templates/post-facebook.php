<?php

?>

<div class="<?php echo esc_attr($args['wrapper_class']); ?>" >
    <div class="page">
        <?php $image = $page->getPicture(); ?>
        <img class="page-picture" src="<?php echo $image->getField('url'); ?>" alt="<?php _e( 'Profile-Picture', 'wpsf' ); ?>" />
        <span class="page-name"><?php echo $page->getName(); ?></span>
    </div>
    <div class="message">
        <?php echo get_facebook_post_message( $social_post ); ?>
    </div>
    <?php if( $social_post['type'] == 'photo' ) { ?>

        <?php 
        $photos = get_facebook_post_photos( $social_post ); 

        if( count( $photos['normal'] ) > 0 ) {
            ?>
            <div class="gallery">
            <?php foreach( $photos['normal'] as $photo ) { ?>
                <img class="photo normal" src="<?php echo $photo['src']; ?>" alt="<?php echo $photo['alt']; ?>" />
            <?php } ?>
            </div>
        <?php } else if( $photos['main'] ) { //single photo
            $photo = $photos['main']; ?>
            <img class="photo main" src="<?php echo $photo['src']; ?>" alt="<?php echo $photo['alt']; ?>" />
        <?php } ?>
        
    <?php } else if( $social_post['type'] == 'video' ) { ?>
        <?php 
        $videos = get_facebook_post_videos( $social_post ); 
        
        if( $videos && $videos['main'] ) { ?> 

            <div class="fb-video" data-href="<?php echo $videos['main']['url']; ?>" data-width="500" data-allowfullscreen="true"></div>

        <?php } ?>

    <?php } else if( $social_post['type'] == 'link' ) { ?>
        <?php 
        $photos = get_facebook_post_photos( $social_post ); 



        if( $photos['main'] ) { //single photo
            $photo = $photos['main']; ?>
            <img class="photo main" src="<?php echo $photo['src']; ?>" alt="<?php echo $photo['alt']; ?>" />
        <?php } else if( $photos['normal'] ) { 
            $photo = $photos['normal'][0]; ?>
            <a href="<?php echo $photo['link']; ?>" target="_blank">
                <img class="photo normal" src="<?php echo $photo['src']; ?>" alt="<?php echo $photo['alt']; ?>" />     
            </a>            
        <?php } else if( count( $photos['sub']) > 0  ) { 
                    //If only one image should be displayed maybe take the forst? 
                    //TODO param for only show one ?>
            <ul class="carousel">
                <?php foreach( $photos['sub'] as $img ) { ?>
                    <li class="carousel-image" >
                        <a href="<?php echo $img['link']; ?>" target="_blank">
                            <img src="<?php echo $img['src']; ?>" alt="<?php echo $img['alt']; ?>" />
                        </a>
                    </li>
                <?php } ?>
            </ul>            
        <?php } ?>
    <?php } ?>
</div>