<?php

?>

<div class="<?php echo esc_attr($args['wrapper_class']); ?>" >
    <?php if( $social_post->user ) { ?>
        <div class="page">
            <img class="user-profile-picture" src="<?php echo $social_post->user->profile_picture ?>" alt="<?php _e( 'Profile-Picture', 'wpsf' ); ?>" />
            <span class="user-name"><?php echo $social_post->user->username; ?></span>
        </div>
    <?php } ?>
    <div class="message">
        <?php $message = get_instagram_post_message( $social_post ); 
        echo $message; ?>
    </div>
    <?php if( $social_post->type == 'image' ) { ?>

        <?php 
        $photo = get_instagram_post_photo( $social_post ); 

        if( $photo) { ?>
            <img class="photo" src="<?php echo $photo; ?>" alt="<?php echo $message; ?>" />
        <?php } ?>
        
    <?php } else if( $social_post->type == 'video' ) { ?>
        <?php 
        $video = get_instagram_post_video( $social_post ); 
        
        if( $video && $video['video'] ) { ?> 

            <video class="instagram-video" post="<?php echo $video['image']; ?>" >
                <source src="<?php echo $video['video']; ?>" type="video/mp4" />
            </video>
        <?php } ?>

    <?php } ?>
</div>