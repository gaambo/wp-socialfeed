<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

final class WP_Social_Feed_Shortcodes {

	private static $_instance = null;

	private $_hook;

	public function __construct () {
        add_shortcode( 'socialfeed', array( $this, 'do_shortcode' ) ); 
    }

	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
    }

	public function do_shortcode ( $args, $content ) {
		$defaults = array(
            'feed' => 'default',
        );

        $args = wp_parse_args( $args, $defaults );

        try {
            $feed = new WP_Social_Feed_Feed( $args['feed'], $args ); 
            $output = $feed->get_feed( $args ); 
            return $output; 
        } catch( Exception $ex ) {
            if( WP_DEBUG ) {
                return $this->error( $ex->getMessage() );
            } else {
                return $this->error( 'An Error occurred.' ); 
            }
             
        } 
    }

    protected function error( $error ) {
        return '<p class="debug error">' . $error . '</p>'; 
    }
}