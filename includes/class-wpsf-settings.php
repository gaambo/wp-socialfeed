<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

final class WP_Social_Feed_Settings {

	private static $_instance = null;
	
	protected $settings_sections; 
	protected $settings_fields; 

	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
    }

	public function __construct () {
		$this->init_settings(); 
	}

	protected function init_settings() {
		// SECTIONS
		$settings_sections = array();
		$settings_sections['general'] = __( 'Allgemein', 'wpsf' ); 
		// Add your new sections below here.
		// Admin tabs will be created for each section.
		// Don't forget to add fields for the section in the get_settings_fields() function below
		$this->settings_sections = (array)apply_filters( 'wpsf/settings-sections', $settings_sections );

		//SETTINGS FIELDS
		$settings_fields = array();
		// Declare the default settings fields.

		$settings_fields['general'] = array(
			'cache_expire' => array(
				'name' => __( 'Standard Cache Gültigkeit', 'wpsf' ), 
				'type' => 'input', 
				'default' => 3600, 
				'input_type' => 'number', 
				'attributes' => array(
					
				),
				'section' => 'general',
			)
			);

		$this->settings_fields = (array)apply_filters( 'wpsf/settings-fields', $settings_fields );
	}

	public function validate_settings ( $input, $section ) {
		if ( is_array( $input ) && 0 < count( $input ) ) {
			$fields = $this->get_settings_fields( $section );
			foreach ( $input as $k => $v ) {
				if ( ! isset( $fields[$k] ) ) {
					continue;
				}
				// Determine if a method is available for validating this field.
				$method = 'validate_field_' . $fields[$k]['type'];
				if ( ! method_exists( $this, $method ) ) {
					if ( true === (bool)apply_filters( 'wpsf/validate-field-' . $fields[$k]['type'] . '_use_default', true ) ) {
						$method = 'validate_field_text';
					} else {
						$method = '';
					}
				}
				// If we have an internal method for validation, filter and apply it.
				if ( '' != $method ) {
					add_filter( 'wpsf/validate-field-' . $fields[$k]['type'], array( $this, $method ) );
				}
				$method_output = apply_filters( 'wpsf/validate-field-' . $fields[$k]['type'], $v, $fields[$k] );
				if ( ! is_wp_error( $method_output ) ) {
					$input[$k] = $method_output;
				}
			}
		}
		return $input;
    }

	public function validate_field_text ( $v ) {
		return (string)wp_kses_post( $v );
	}
	
	public function validate_field_input ( $v ) {
		return $v;
    }

	public function validate_field_textarea ( $v ) {
		// Allow iframe, object and embed tags in textarea fields.
		$allowed 			= wp_kses_allowed_html( 'post' );
		$allowed['iframe'] 	= array(
								'src' 		=> true,
								'width' 	=> true,
								'height' 	=> true,
								'id' 		=> true,
								'class' 	=> true,
								'name' 		=> true
								);
		$allowed['object'] 	= array(
								'src' 		=> true,
								'width' 	=> true,
								'height' 	=> true,
								'id' 		=> true,
								'class' 	=> true,
								'name' 		=> true
								);
		$allowed['embed'] 	= array(
								'src' 		=> true,
								'width' 	=> true,
								'height' 	=> true,
								'id' 		=> true,
								'class' 	=> true,
								'name' 		=> true
								);
		return wp_kses( $v, $allowed );
    }

	public function validate_field_checkbox ( $v ) {
		if ( 'true' != $v ) {
			return 'false';
		} else {
			return 'true';
		}
    }

	public function validate_field_url ( $v ) {
		return trim( esc_url( $v ) );
    }

	public function render_field ( $args ) {
		$args = apply_filters( 'wpsf/render-settings-field/' . $args['section'] . '/' . $args['id'], $args ); 
		$html = '';
		if ( ! in_array( $args['type'], $this->get_supported_fields() ) ) return ''; // Supported field type sanity check.
		// Make sure we have some kind of default, if the key isn't set.
		if ( ! isset( $args['default'] ) ) {
			$args['default'] = '';
		}
		$method = 'render_field_' . $args['type'];
		if ( ! method_exists( $this, $method ) ) {
			$method = 'render_field_text';
		}
		// Construct the key.
		$key 				= 'wpsf' . '-' . $args['section'] . '[' . $args['id'] . ']';
		$method_output 		= $this->$method( $key, $args );
		if ( ! is_wp_error( $method_output ) ) {
			$html .= $method_output;
		}
		// Output the description, if the current field allows it.
		if ( isset( $args['type'] ) && ! in_array( $args['type'], (array)apply_filters( 'wpsf/no-description-fields', array( 'checkbox' ) ) ) ) {
			if ( isset( $args['description'] ) ) {
				$description = '<p class="description">' . wp_kses_post( $args['description'] ) . '</p>' . "\n";
				if ( in_array( $args['type'], (array)apply_filters( 'wpsf/new-line-description-fields', array( 'textarea', 'select' ) ) ) ) {
					$description = wpautop( $description );
				}
				$html .= $description;
			}
		}
		echo $html;
    }

	public function get_settings_sections () {
		return $this->settings_sections; 
    }

	public function get_settings_fields ( $section ) {
		if(isset( $this->settings_fields[$section] ) ) {
			return $this->settings_fields[$section]; 
		}
		return array(); 
    }

	protected function render_field_text ( $key, $args ) {
		$html = '<input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" size="40" type="text" value="' . esc_attr( $this->get_value( $args['id'], $args['default'], $args['section'] ) ) . '" />' . "\n";
		return $html;
	}
	
	protected function render_field_input( $key, $args ) {
		$type = isset( $args['input_type'] ) ? $args['input_type'] : 'text'; 
		$attrs = isset( $args['attributes'] ) ? $args['attributes'] : array(); 

		$html = '<input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" size="40" type="' . $type . '" value="' . esc_attr( $this->get_value( $args['id'], $args['default'], $args['section'] ) ) . '" ' . wpsf_esc_html_attrs( $attrs ) . ' />' . "\n";
		return $html;
	}

	protected function render_field_radio ( $key, $args ) {
		$html = '';
		if ( isset( $args['options'] ) && ( 0 < count( (array)$args['options'] ) ) ) {
			$html = '';
			foreach ( $args['options'] as $k => $v ) {
				$html .= '<input type="radio" name="' . esc_attr( $key ) . '" value="' . esc_attr( $k ) . '"' . checked( esc_attr( $this->get_value( $args['id'], $args['default'], $args['section'] ) ), $k, false ) . ' /> ' . esc_html( $v ) . '<br />' . "\n";
			}
		}
		return $html;
    }

	protected function render_field_textarea ( $key, $args ) {
		// Explore how best to escape this data, as esc_textarea() strips HTML tags, it seems.
		$html = '<textarea id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" cols="42" rows="5">' . $this->get_value( $args['id'], $args['default'], $args['section'] ) . '</textarea>' . "\n";
		return $html;
    }

	protected function render_field_checkbox ( $key, $args ) {
		$has_description = false;
		$html = '';
		if ( isset( $args['description'] ) ) {
			$has_description = true;
			$html .= '<label for="' . esc_attr( $key ) . '">' . "\n";
		}
		$html .= '<input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" type="checkbox" value="true"' . checked( esc_attr( $this->get_value( $args['id'], $args['default'], $args['section'] ) ), 'true', false ) . ' />' . "\n";
		if ( $has_description ) {
			$html .= wp_kses_post( $args['description'] ) . '</label>' . "\n";
		}
		return $html;
    }

	protected function render_field_select ( $key, $args ) {
		$this->_has_select = true;
		$html = '';
		if ( isset( $args['options'] ) && ( 0 < count( (array)$args['options'] ) ) ) {
			$html .= '<select id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '">' . "\n";
				foreach ( $args['options'] as $k => $v ) {
					$html .= '<option value="' . esc_attr( $k ) . '"' . selected( esc_attr( $this->get_value( $args['id'], $args['default'], $args['section'] ) ), $k, false ) . '>' . esc_html( $v ) . '</option>' . "\n";
				}
			$html .= '</select>' . "\n";
		}
		return $html;
    }

	protected function render_field_select_taxonomy ( $key, $args ) {
		$this->_has_select = true;
		$defaults = array(
			'show_option_all'    => '',
			'show_option_none'   => '',
			'orderby'            => 'ID',
			'order'              => 'ASC',
			'show_count'         => 0,
			'hide_empty'         => 1,
			'child_of'           => 0,
			'exclude'            => '',
			'selected'           => $this->get_value( $args['id'], $args['default'], $args['section'] ),
			'hierarchical'       => 1,
			'class'              => 'postform',
			'depth'              => 0,
			'tab_index'          => 0,
			'taxonomy'           => 'category',
			'hide_if_empty'      => false,
			'walker'             => ''
        );
		if ( ! isset( $args['options'] ) ) {
			$args['options'] = array();
		}
		$args['options'] 			= wp_parse_args( $args['options'], $defaults );
		$args['options']['echo'] 	= false;
		$args['options']['name'] 	= esc_attr( $key );
		$args['options']['id'] 		= esc_attr( $key );
		$html = '';
		$html .= wp_dropdown_categories( $args['options'] );
		return $html;
    }

	public function get_array_field_types () {
		return array();
    }

	protected function get_no_label_field_types () {
		return array( 'info' );
    }

	public function get_supported_fields () {
		return (array)apply_filters( 'wpsf/supported-fields', array( 'text', 'checkbox', 'radio', 'textarea', 'select', 'select_taxonomy', 'input' ) );
    }

	public function get_value ( $key, $default, $section ) {
		$values = get_option( 'wpsf-' . $section, array() );
		if ( is_array( $values ) && isset( $values[$key] ) ) {
			$response = $values[$key];
		} else {
			$response = $default;
		}
		return $response;
    }

	public function get_settings ( $section = '' ) {
		$response = false;
		$sections = array_keys( (array)$this->get_settings_sections() );
		if ( in_array( $section, $sections ) ) {
			$sections = array( $section );
		}
		if ( 0 < count( $sections ) ) {
			foreach ( $sections as $k => $v ) {
				$fields = $this->get_settings_fields( $v );
				$values = get_option( 'wpsf/' . $v, array() );
				if ( is_array( $fields ) && 0 < count( $fields ) ) {
					foreach ( $fields as $i => $j ) {
						// If we have a value stored, use it.
						if ( isset( $values[$i] ) ) {
							$response[$i] = $values[$i];
						} else {
							// Otherwise, check for a default value. If we have one, use it. Otherwise, return an empty string.
							if ( isset( $fields[$i]['default'] ) ) {
								$response[$i] = $fields[$i]['default'];
							} else {
								$response[$i] = '';
							}
						}
					}
				}
			}
		}
		return $response;
    }
    
} 