<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

final class WP_Social_Feed_Enqueues {

	private static $_instance = null;

	private $_hook;

	public function __construct () {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) ); 
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) ); 
    }

	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
    }

    public function enqueue_scripts() {
        //do default enqueues
        //add properties to $enqueues like: array(
        //    handle, src, deps, ver, footer
        //)
        
        $enqueues = array(); 
        $enqueues = apply_filters( 'wpsf/enqueues/scripts', $enqueues );
        $this->enqueue( $enqueues, 'scripts' );  

        $registers = array(); 
        $registers = apply_filters( 'wpsf/enqueues/register/scripts', $registers );
        $this->register( $registers, 'scripts' ); 
    }

    public function enqueue_styles() {
        //do default enqueues
        //add properties to $enqueues like: array(
        //    handle, src, deps, ver, media
        //)
        
        $enqueues = array(); 
        $enqueues = apply_filters( 'wpsf/enqueues/styles', $enqueues );        
        $this->enqueue( $enqueues, 'styles' ); 

        $registers = array(); 
        $registers = apply_filters( 'wpsf/enqueues/register/scripts', $registers );
        $this->register( $registers, 'scripts' ); 
    }

    protected function enqueue( $enqueues, $what ) {
        $func = $what == 'styles' ? 'wp_enqueue_style' : 'wp_enqueue_script'; 

        foreach( $enqueues as $enq ) {
            call_user_func_array( $func, $enq ); 
        }
    }

    protected function register( $registers, $what ) {
        $func = $what == 'styles' ? 'wp_register_style' : 'wp_register_script'; 

        foreach( $registers as $reg ) {
            call_user_func_array( $func, $reg ); 
        }
    }

    protected function error( $error ) {
        return '<p class="debug error">' . $error . '</p>'; 
    }
}