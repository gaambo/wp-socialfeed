<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

final class WP_Social_Feed_Admin {

	private static $_instance = null;

	private $_hook;

	public function __construct () {
		// Register the settings with WordPress.
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		// Register the settings screen within WordPress.
		add_action( 'admin_menu', array( $this, 'register_settings_screen' ) );
    }
    
	public static function instance () {
		if ( is_null( self::$_instance ) )
			self::$_instance = new self();
		return self::$_instance;
    }
    
	public function register_settings_screen () {
		$this->_hook = add_submenu_page( 'options-general.php', __( 'Social Feed Settings', 'wpsf' ), __( 'Social Feed', 'wpsf' ), 'manage_options', 'wpsf', array( $this, 'settings_screen' ) );
    }
    
	public function settings_screen () {
		global $title;
		$sections = WP_Social_Feed()->settings->get_settings_sections();
		$tab = $this->_get_current_tab( $sections );
		?>
		<div class="wrap wpsf-wrap">
			<?php
				echo $this->get_admin_header_html( $sections, $title );
			?>
			<form action="options.php" method="post">
				<?php
					settings_fields( 'wpsf-settings-' . $tab );
					do_settings_sections( 'wpsf-' . $tab );
					submit_button( __( 'Save Changes', 'wpsf' ) );
				?>
			</form>
		</div><!--/.wrap-->
		<?php
    }

	public function register_settings () {
		$sections = WP_Social_Feed()->settings->get_settings_sections();
		if ( 0 < count( $sections ) ) {
			foreach ( $sections as $k => $v ) {
				register_setting( 'wpsf-settings-' . sanitize_title_with_dashes( $k ), 'wpsf-' . $k, array( $this, 'validate_settings' ) );
				add_settings_section( sanitize_title_with_dashes( $k ), $v, array( $this, 'render_settings' ), 'wpsf-' . $k, $k, $k );
			}
		}
    }

	public function render_settings ( $args ) {
		$token = $args['id'];
		$fields = WP_Social_Feed()->settings->get_settings_fields( $token );
		if ( 0 < count( $fields ) ) {
			foreach ( $fields as $k => $v ) {
				$args 		= $v;
				$args['id'] = $k;
				$args['section'] = $token;
				add_settings_field( $k, $v['name'], array( WP_Social_Feed()->settings, 'render_field' ), 'wpsf-' . $token , $token, $args );
			}
		}
    }

	public function validate_settings ( $input ) {
		$sections = WP_Social_Feed()->settings->get_settings_sections();
		$tab = $this->_get_current_tab( $sections );
		return WP_Social_Feed()->settings->validate_settings( $input, $tab );
    }

	public function get_admin_header_html ( $sections, $title ) {
		$defaults = array(
							'tag' => 'h2',
							'atts' => array( 'class' => 'wpsf-wrapper' ),
							'content' => $title
						);
		$args = $this->_get_admin_header_data( $sections, $title );
		$args = wp_parse_args( $args, $defaults );
		$atts = '';
		if ( 0 < count ( $args['atts'] ) ) {
			foreach ( $args['atts'] as $k => $v ) {
				$atts .= ' ' . esc_attr( $k ) . '="' . esc_attr( $v ) . '"';
			}
		}
		$response = '<' . esc_attr( $args['tag'] ) . $atts . '>' . $args['content'] . '</' . esc_attr( $args['tag'] ) . '>' . "\n";
		return $response;
    }

	private function _get_current_tab ( $sections = array() ) {
		if ( isset ( $_GET['tab'] ) ) {
			$response = sanitize_title_with_dashes( $_GET['tab'] );
		} else {
			if ( is_array( $sections ) && ! empty( $sections ) ) {
				list( $first_section ) = array_keys( $sections );
				$response = $first_section;
			} else {
				$response = '';
			}
		}
		return $response;
    }

	private function _get_admin_header_data ( $sections, $title ) {
		$response = array( 'tag' => 'h2', 'atts' => array( 'class' => 'wpsf-wrapper' ), 'content' => $title );
		if ( is_array( $sections ) && 1 < count( $sections ) ) {
			$response['content'] = '';
			$response['atts']['class'] = 'nav-tab-wrapper';
			$tab = $this->_get_current_tab( $sections );
			foreach ( $sections as $key => $value ) {
				$class = 'nav-tab';
				if ( $tab == $key ) {
					$class .= ' nav-tab-active';
				}
				$response['content'] .= '<a href="' . admin_url( 'options-general.php?page=wpsf&tab=' . sanitize_title_with_dashes( $key ) ) . '" class="' . esc_attr( $class ) . '">' . esc_html( $value ) . '</a>';
			}
		}
		return (array)apply_filters( 'wpsf/get-admin-header-data', $response );
	}
}