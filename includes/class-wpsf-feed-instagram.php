<?php

//TODO set redirects url 

if( !defined( 'ABSPATH' ) ) exit; 

if( !class_exists( 'WP_Social_Feed_Feed_Instagram' ) ) {

    class WP_Social_Feed_Feed_Instagram extends WP_Social_Feed_Feed_Abstract {

        protected $settings; 


        protected static $api_version = 'v2.11'; 

        protected $ig_api; 

        protected $args; 
        protected $user; 

        public static function init() {
            //needs to be called in init, because we need to register(!) the script before wp_enqueue_scripts so we can enqueue it later in the content
            add_filter( 'wpsf/enqueues/register/scripts', array( __CLASS__, 'register_scripts' ) ); 
            add_filter( 'wpsf/settings-sections', array( __CLASS__, 'register_settings_sections' ) ); 
            add_filter( 'wpsf/settings-fields', array( __CLASS__, 'register_settings_fields' ) ); 
            add_filter( 'wpsf/render-settings-field/instagram/access_token', array( __CLASS__, 'render_access_token_setting_field' ) );
            add_filter( 'admin_menu', array( __CLASS__, 'add_admin_page' ) ); 
        }

        public function __construct( $args = array() ) {
            $this->settings = array(
            ); 

            $ig_api = self::build_api(); 
            if( $ig_api ) {
                $this->ig_api = $ig_api; 
            } else {
                return false; 
            }

            $this->args = wp_parse_args( $args, array(
                'limit' => 5,
                'page_id' => false,
                'before' => false, 
                'after' => false,
            ) );      
        }

        protected static function build_api() {
            $plugin = WP_Social_Feed(); 


            $app_id = $plugin->settings->get_value( 'app_id', null, 'instagram' ); 
            $app_secret = $plugin->settings->get_value( 'app_secret', null, 'instagram' ); 
            $access_token = $plugin->settings->get_value( 'access_token', null, 'instagram' ); 

            $callback_url = add_query_arg( array(
                'page' => 'wpsf-instagram',
                //'tab' => 'instagram'
            ), admin_url( 'options.php' ) );

            try {
                $ig_api = new Andreyco\Instagram\Client(array(
                    'apiKey' => $app_id, 
                    'apiSecret' => $app_secret, 
                    'apiCallback' => $callback_url,
                ));
                $ig_api->setAccessToken($access_token);
                $ig_api->setEnforceSignedRequests(true); 

                return $ig_api;
            } catch( Exception $exception ) {
                return self::handle_error($exception); 
            }
        }
        
        public function get_posts( $args ) {
            //BUILD ARGS & URL 

            if( !$this->ig_api ) {
                return false; 
            }

            $args = wp_parse_args( $args, $this->args );

            if( $args['limit'] > 90 ) { //instagram limit
                $new_limit = ($args['limit'] - 90);
                if( $new_limit <= 90) {
                    $args['limit'] = $new_limit; 
                } else {
                    while( $new_limit > 90 ) {
                        $new_limit = $new_limit - 90; 
                    }
                    $args['limit'] = $new_limit; 
                }
            }

            try {
                if( isset( $args['next_page_url'] ) && isset( $args['next_page_max_id'] ) ) {
                    $pagination = (object) array(
                        'pagination' => (object) array(
                            'next_url' => $args['next_page_url'] . '?fake=true', //cause gets split by ? 
                            'next_max_id' => $args['next_page_max_id']
                        )                        
                    ); 
                    $result = $this->ig_api->pagination( $pagination, $args['limit'] );                     
                }

                if( !isset( $result ) || !$result ) {
                    $result = $this->ig_api->getUserMedia( 'self', $args['limit'] ); 
                }
                
                if(!$result ) {
                    return false; 
                }
            } catch( Exception $exception ) {
                self::handle_error($exception); 
                return false; 
            }

            $return = array(
                'posts' => $result->data
            ); 
            
            //must be split here - because otherwise we output our access token 
            if( $result->pagination && isset( $result->pagination->next_url ) ) {
                $pagination_url = parse_url( $result->pagination->next_url ); 
                $pagination_params = array(); 
                parse_str( $pagination_url['query'], $pagination_params ); 
                $return['next_page_url'] = $pagination_url['scheme'] . '://' . $pagination_url['host'] . $pagination_url['path']; 
                $return['next_page_max_id'] = $pagination_params['max_id']; 
            }
            
            return $return; 
        }

        public function get_single_post( $args ) {
            if( !$this->ig_api ) {
                return false; 
            }

            //BUILD ARGS & URL 
            $args = wp_parse_args( $args, $this->args );

            if( !$args['post_id'] ) {
                return false;
            }


            try {
                $result = $this->ig_api->getMedia($args['post_id']); 
            } catch( \Facebook\Exceptions\FacebookResponseException $ex ) {
                self::handle_error( $ex ); 
                return false; 
            } catch( \Facebook\Exceptions\FacebookSDKException $ex ) {
                self::handle_error( $ex ); 
                return false; 
            }
                
            if( $result ) {
                return $result->data; 
            } else {
                return false; 
            }
        }

        public function display_post( $social_post, $args ) {
            $default = wp_parse_args( $this->args, array(
                'wrapper_class' => 'instagram-post',
            ) ); 
            $args = wp_parse_args( $args, $default );

            //Instagram gives poster in user field with media
            //$user = $this->get_user_data(); 

            if( $social_post->type ) {
                include wpsf_get_template( 'post-instagram' , $social_post->type );
            } else {
                include wpsf_get_template( 'post-instagram' ); 
            }
            
        }

        public function get_user_data() {
            if( !$this->user ) {
                //BUILD ARGS & URL 

                try {
                    $result = $this->ig_api->getUser('self');
                    if( $result ) {
                        $this->user = $result->data; 
                    }
                } catch( \Facebook\Exceptions\FacebookResponseException $ex ) {
                    self::handle_error( $ex ); 
                    return false; 
                } catch( \Facebook\Exceptions\FacebookSDKException $ex ) {
                    self::handle_error( $ex ); 
                    return false; 
                }            
                
                
            }

            return $this->page; 
            
        }

        public static function register_scripts( $registers ) {
            return $registers;
        }

        public static function register_settings_sections( $settings_sections ) {
            $settings_sections['instagram'] = __( 'Instagram', 'wpsf' );
            return $settings_sections; 
        }

        public static function register_settings_fields( $settings_fields ) {
           
            $settings_fields['instagram'] = array(
                'app_id' => array(
                    'name' => __( 'Instagram App ID', 'wpsf' ), 
                    'type' => 'text', 
                    'default' => '',
                    'description' => __( 'Instagram App ID', 'wpsf'), 
                ),
                'app_secret' => array(
                    'name' => __( 'Instagram App Secret', 'wpsf' ), 
                    'type' => 'text', 
                    'default' => '',
                    'description' => __( 'Instagram App Secret', 'wpsf'), 
                ), 
                'access_token' => array(
                    'name' => __( 'Instagram User Access Token', 'wpsf' ), 
                    'type' => 'text', 
                    'default' => '', 
                    'description' => __( 'Instagram User Access Token', 'wpsf' ),
                ),
                'cache_expire' => array(
                    'name' => __( 'Standard Cache Gültigkeit', 'wpsf' ), 
                    'type' => 'input', 
                    'default' => 3600, 
                    'input_type' => 'number', 
                    'attributes' => array(
                        
                    ),
                )
            );
            
            return $settings_fields; 
        }

        public static function add_admin_page() {
            //null to hide in menu --> see https://stackoverflow.com/questions/3902760/how-do-you-add-a-wordpress-admin-page-without-adding-it-to-the-menu
            add_submenu_page( 'options.php', __( 'Social Feed Instagram Access Token', 'wpsf' ), __( 'Social Feed Instagram Access Token', 'wpsf' ), 'manage_options', 'wpsf-instagram', array( __CLASS__, 'settings_screen' ) );
        }

        public static function settings_screen() {
            $success = false; 
            if( $_SERVER['REQUEST_METHOD'] == 'GET'  ) {
                if( $_GET['code'] ) {
                    $api = self::build_api(); 
                    $data = $api->getOAuthToken($_GET['code']); 

                    if( $data && isset( $data->access_token ) ) {
                        $options = get_option( 'wpsf-instagram' ); 
                        $options['access_token'] = $data->access_token; 
                        update_option( 'wpsf-instagram', $options ); 
                        $success = true; 
                    }
                }
            }

            if( $success ) {
                echo '<p>' . __( 'Added Access Token successfully', 'wpsf' ) . '</p>';  
            } else {
                echo '<p>' . __( 'Error adding Access Token', 'wpsf' ) . '</p>';  
            }

            //redirect does not work because of header already sent 
            //wp_redirect( menu_page_url( 'wpsf', false ), 201 ); 
            //exit;
            //menu_page_url only works in admin 
            $settings_url = menu_page_url( 'wpsf', false ); 
            $settings_url = add_query_arg( 'tab', 'instagram', $settings_url ); 
            echo '<a href="' . $settings_url . '" >' . __( 'Go back to instagram settings', 'wpsf' ) . '</a>'; 
        }

        public static function render_access_token_setting_field( $args ) {
            //menu page url only works in admin 
            $settings_url = menu_page_url( 'wpsf-instagram', false ); 
            //$settings_url = add_query_arg( 'tab', 'instagram', $settings_url );  //not needed according to https://www.instagram.com/developer/authentication/table
            $args['description'] .= '<p>add the following url to the allowed return uris of the instagram app: ' . $settings_url . '</p>'; 
            $api = self::build_api(); 
            if( $api ) {
                $loginUrl = $api->getLoginUrl(); 
                if( $loginUrl ) {
                    $args['description'] .= '<a href="' . $loginUrl . '" >authorize here</a>';
                    return $args; 
                }
            }

            $args['description'] .= ' enter id & secret and save';

            return $args; 
            
        }
        
    }
}

add_filter( 'wpsf/feeds', function($social_feeds) {
    $social_feeds['instagram'] = 'WP_Social_Feed_Feed_Instagram'; 
    return $social_feeds;
}, 1 ); 

WP_Social_Feed_Feed_Instagram::init(); 