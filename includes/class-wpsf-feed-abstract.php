<?php

if( !defined( 'ABSPATH' ) ) exit; 

if( !interface_exists( 'WP_Social_Feed_Feed_Interface' ) ) {
    interface WP_Social_Feed_Feed_Interface {
        public function get_posts( $args ); 
        public function get_single_post( $args ); 
        public function display_post( $social_post, $args ); 
    }
}

if( !class_exists( 'WP_Social_Feed_Feed_Abstract' ) ) {

    abstract class WP_Social_Feed_Feed_Abstract implements WP_Social_Feed_Feed_Interface {
        
        public abstract function get_posts( $args );

        public abstract function get_single_post( $args );

        public abstract function display_post( $social_post, $args );

        //may be called by plugin to init things like enqueues & filters 
        public static function init() {}

        protected static function handle_error( $exception ) {
            if( WP_DEBUG ) {
                error_log( $exception->getMessage() ); 
            }

            return false; 
        }

    }
}