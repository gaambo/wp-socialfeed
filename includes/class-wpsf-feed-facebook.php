<?php

if( !defined( 'ABSPATH' ) ) exit; 

if( !class_exists( 'WP_Social_Feed_Feed_Facebook' ) ) {

    class WP_Social_Feed_Feed_Facebook extends WP_Social_Feed_Feed_Abstract {

        protected $settings; 


        protected static $api_version = 'v2.11'; 

        protected $fb_api; 

        protected $args; 
        protected $page; 

        public static function init() {
            //needs to be called in init, because we need to register(!) the script before wp_enqueue_scripts so we can enqueue it later in the content
            add_filter( 'wpsf/enqueues/register/scripts', array( __CLASS__, 'register_scripts' ) ); 
            add_filter( 'wpsf/settings-sections', array( __CLASS__, 'register_settings_sections' ) ); 
            add_filter( 'wpsf/settings-fields', array( __CLASS__, 'register_settings_fields' ) ); 
        }

        public function __construct( $args = array() ) {
            $plugin = WP_Social_Feed(); 
            $this->settings = array(
            ); 

            $app_id = $plugin->settings->get_value( 'app_id', null, 'facebook' ); 
            $app_secret = $plugin->settings->get_value( 'app_secret', null, 'facebook' ); 

            try {
                $this->fb_api = new \Facebook\Facebook(array(
                    'app_id' => $app_id,
                    'app_secret' => $app_secret,
                    'default_graph_version' => self::$api_version,
                    'default_access_token' => $app_id . '|' . $app_secret,
                )); 
            } catch( FacebookSDKException $exception ) {
                return self::handle_error($exception); 
            }

            $this->args = wp_parse_args( $args, array(
                'limit' => 5,
                'page_id' => false,
                'fields' => array( 'attachments' ,'message' , 'link' , 'from' , 'type' , 'name' , 'caption' , 'created_time' , 'description' , 'picture' ),
                'from' => 'page', //only posts from page
                'before' => false, 
                'after' => false,
            ) );      

            wp_enqueue_script('facebook-js-sdk'); 
        }
        
        public function get_posts( $args ) {
            //BUILD ARGS & URL 
            $args = wp_parse_args( $args, $this->args );

            if( $args['limit'] > 100 ) { //facebook limit
                $new_limit = ($args['limit'] - 100);
                if( $new_limit <= 100) {
                    $args['limit'] = $new_limit; 
                } else {
                    while( $new_limit > 100 ) {
                        $new_limit = $new_limit - 100; 
                    }
                    $args['limit'] = $new_limit; 
                }
            }

            if( !$args['page_id'] ) {
                return false;
            }

            $args['fields'] = wpsf_whitelist_array( $args['fields'], array( 'attachments' ,'message' ,' link' , 'from' , 'type' , 'name' , 'caption' , 'created_time' , 'description' , 'full_picture', 'picture' ) ); 

            $path = '/feed'; 
            if( $args['from'] == 'page' ) {
                $path = '/posts'; //only posts from page 
            }
            
            $request_url_args = array(
                'fields' => implode( ',', $args['fields'] ), 
                'limit' => $args['limit'], 
            ); 

            if( $args['before'] ) {
                $request_url_args['before'] = $args['before']; 
            }

            if( $args['after'] ) {
                $request_url_args['after'] = $args['after']; 
            } else if( isset( $args['next_page'] ) ) {
                $request_url_args['after'] = $args['next_page']; 
            }

            $request_url = $this->build_request_url( $args['page_id'] . $path, $request_url_args ); 

            try {
                $response = $this->fb_api->get($request_url);
                if( !$response ) {
                    return false; 
                }
            } catch( \Facebook\Exceptions\FacebookResponseException $ex ) {
                self::handle_error( $ex ); 
                return false; 
            } catch( \Facebook\Exceptions\FacebookSDKException $ex ) {
                self::handle_error( $ex ); 
                return false; 
            }
             
            $feedEdge = $response->getGraphEdge(); 
            return array(
                'posts' => $feedEdge, 
                'next_page' => $feedEdge->getNextCursor(),
            );
        }

        public function get_single_post( $args ) {
            //BUILD ARGS & URL 
            $args = wp_parse_args( $args, $this->args );

            if( !$args['post_id'] ) {
                return false;
            }

            $args['fields'] = wpsf_whitelist_array( $args['fields'], array( 'attachments' ,'message' ,' link' , 'from' , 'type' , 'name' , 'caption' , 'created_time' , 'description' , 'full_picture', 'picture'  ) ); 
            
            $request_url_args = array(
                'fields' => implode( ',', $args['fields'] ), 
                'limit' => $args['limit'], 
            ); 

            $request_url = $this->build_request_url( $args['post_id'], $request_url_args ); 

            try {
                $response = $this->fb_api->get($request_url);
            } catch( \Facebook\Exceptions\FacebookResponseException $ex ) {
                self::handle_error( $ex ); 
                return false; 
            } catch( \Facebook\Exceptions\FacebookSDKException $ex ) {
                self::handle_error( $ex ); 
                return false; 
            }
             
            $fb_post = $response->getGraphObject(); 
            return $fb_post->asArray();
        }

        public function display_post( $social_post, $args ) {
            $default = wp_parse_args( $this->args, array(
                'wrapper_class' => 'facebook-post',
            ) ); 
            $args = wp_parse_args( $args, $default );

            $page = $this->get_page_data();

            if( $social_post->getField('type') ) {
                include wpsf_get_template( 'post-facebook' , $social_post->getField('type') );
            } else {
                include wpsf_get_template( 'post-facebook' ); 
            }
            
        }

        public function get_page_data() {
            if( !$this->page ) {
                //BUILD ARGS & URL 

                if( !$this->args['page_id'] ) {
                    return false;
                }
                
                $request_url_args = array(
                    'fields' => 'name,picture', 
                ); 

                $request_url = $this->build_request_url( $this->args['page_id'], $request_url_args ); 

                try {
                    $response = $this->fb_api->get($request_url);
                    if( $response ) {
                        $this->page = $response->getGraphPage(); 
                    }
                } catch( \Facebook\Exceptions\FacebookResponseException $ex ) {
                    self::handle_error( $ex ); 
                    return false; 
                } catch( \Facebook\Exceptions\FacebookSDKException $ex ) {
                    self::handle_error( $ex ); 
                    return false; 
                }
                
                
            }

            return $this->page; 
            
        }

        public static function register_scripts( $registers ) {
            $registers[] = array(
                'facebook-js-sdk', 
                'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5', 
                array(), 
                '2.5',
                'screen'
            );

            return $registers;
        }

        public static function register_settings_sections( $settings_sections ) {
            $settings_sections['facebook'] = __( 'Facebook', 'wpsf' );
            return $settings_sections; 
        }

        public static function register_settings_fields( $settings_fields ) {
           
            $settings_fields['facebook'] = array(
                'app_id' => array(
                    'name' => __( 'Facebook App ID', 'wpsf' ), 
                    'type' => 'text', 
                    'default' => '',
                    'description' => __( 'Facebook App ID', 'wpsf'), 
                ),
                'app_secret' => array(
                    'name' => __( 'Facebook App Secret', 'wpsf' ), 
                    'type' => 'text', 
                    'default' => '',
                    'description' => __( 'Facebook App Secret', 'wpsf'), 
                ), 
                'cache_expire' => array(
                    'name' => __( 'Standard Cache Gültigkeit', 'wpsf' ), 
                    'type' => 'input', 
                    'default' => 3600, 
                    'input_type' => 'number', 
                    'attributes' => array(
                        
                    ),
                )
            );
            
            return $settings_fields; 
        }

        // protected static function make_request( $url, $check_data = false ) {
        //     $curl = curl_init(); 
        //     curl_setopt( $curl, CURLOPT_URL, $url ); 
        //     curl_setopt( $curl, CURLOPT_HEADER, 0 ); 
        //     curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true ); 

        //     $response = curl_exec( $curl ); 

        //     if( $response === false ) {
        //         $error = curl_error( $curl ); 
        //         if( WP_DEBUG ) {
        //             error_log( $error ); 
        //         }
        //         return false; 
        //     }

        //     curl_close( $curl ); 

        //     $json_response = json_decode( $response, true ); 

        //     if( isset( $json_response['error'] ) ) {
        //         return false; 
        //     }

        //     if( $check_data ) {
        //         if(!isset( $json_response['data'] ) || empty( $json_response['data'] )) {
        //             return false; 
        //         }
        //     }
        //     return $json_response; 
        // }

        protected function build_request_url( $path, $query ) {
            $url = $path;
            //add_query_arg re-encodes query string - but pipe for facebook has to stay in it 
            $url = add_query_arg( $query, $url ); 

            return $url; 
        }
        
    }
}

add_filter( 'wpsf/feeds', function($social_feeds) {
    $social_feeds['facebook'] = 'WP_Social_Feed_Feed_Facebook'; 
    return $social_feeds;
}, 1 ); 

WP_Social_Feed_Feed_Facebook::init(); 
