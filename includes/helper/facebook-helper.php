<?php

if( !defined( 'ABSPATH' ) ) exit; 

if( !function_exists( 'get_facebook_post_message' ) ) {
    function get_facebook_post_message( $fb_post ) {
        $message = ''; 

        if( $fb_post->getField('message') ) {
            $message = $fb_post->getField('message');
        } else if( $fb_post->getField('name') ) {
            $message = $fb_post->getField('name'); 
        } else if( $fb_post->getField('description') ) {
            $message = $fb_post->getField('description'); 
        }
        
        return $message; 
    }
}

if( !function_exists( 'get_facebook_post_photos' ) ) {
    function get_facebook_post_photos( $fb_post ) {

        $photos = array(
            'main' => false, 
            'sub' => array(), 
            'normal' => array(), 
        ); 

        if( $fb_post->getField('full_picture') ) {
            $photos['main'] = array(
                'src' => $fb_post->getField('picture'), 
                'alt' => $fb_post->getField('name'),
            ); 
        }

        if( $fb_post->getField('attachments') ) {
            $attachments = $fb_post->getField('attachments'); 

            foreach( $attachments as $attachment ) {
                if( isset( $attachment['subattachments'] ) ) {
                    foreach( $attachment['subattachments'] as $subattachment ) {
                        $photos['sub'][] = array(
                            'src' => $subattachment['media']['image']['src'], 
                            'link' => $subattachment['url'], 
                            'alt' => wpsf_maybe_get( 'description', $subattachment, '' ), 
                        );
                    }
                } else {
                    $photos['normal'][] = array(
                        'src' => $attachment['media']['image']['src'], 
                        'link' => $attachment['url'], 
                        'alt' => wpsf_maybe_get( 'description', $attachment, '' ) , 
                    );
                }
            }
        }        

        return $photos;         
        
    }
}

if( !function_exists( 'get_facebook_post_videos' ) ) {
    function get_facebook_post_videos( $fb_post ) {
        $videos = array(
            'picture' => false,
            'main' => false, 
            'normal' => array(), 
        ); 

        if( $fb_post->getField('full_picture' ) ) {
            $photos['picture'] = array(
                'src' => $fb_post->getField('full_picture'), 
                'alt' => $fb_post->getField('name'),
            ); 
        } else if( $fb_post->getField('picture' ) ) {
            $photos['picture'] = array(
                'src' => $fb_post->getField('picture'), 
                'alt' => $fb_post->getField('name'),
            ); 
        }

        if( $fb_post->getField('attachments') ) {
            $attachments = $fb_post->getField('attachments');

            foreach( $attachments as $attachment ) {
                $video = array(
                    'id' => $attachment->getField('target')->getField('id'),
                    'alt' => $attachment->getField('title'), 
                    'url' => $attachment->getField('url'),
                );
                if( !$videos['main'] ) {
                    $videos['main'] = $video;
                } else {
                    $videos['normal'][] = $video; 
                }
            }
        }

        return $videos; 
    }
}

