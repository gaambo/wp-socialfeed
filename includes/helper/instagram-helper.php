<?php

if( !defined( 'ABSPATH' ) ) exit; 

if( !function_exists( 'get_instagram_post_message' ) ) {
    function get_instagram_post_message( $social_post ) {
        if( isset( $social_post->caption ) && $social_post->caption ) {
            if( $social_post->caption->text ) {
                return $social_post->caption->text; 
            }
        }
        return ''; 
    }
}

if( !function_exists( 'get_instagram_post_photo' ) ) {
    function get_instagram_post_photo( $social_post ) {
        $photo = false; 

        if( isset( $social_post->images ) ) {
            if( isset( $social_post->images->standard_resolution ) ) {
                $photo = $social_post->images->standard_resolution->url; 
            } else if( isset( $social_post->images->low_resolution ) ) {
                $photo = $social_post->images->low_resolution->url; 
            } else if( isset( $social_post->images->thumbnail ) ) {
                $photo = $social_post->images->thumbnail->url; 
            } 
        }

        return $photo; 
    }
}

if( !function_exists( 'get_instagram_post_video' ) ) {
    function get_instagram_post_video( $social_post ) {
        $data = array(
            'video' => false, 
            'image' => false,
        ); 

        if( isset( $social_post->videos ) ) {
            if( isset( $social_post->videos->standard_resolution ) ) {
                $data['video'] = $social_post->videos->standard_resolution->url; 
            } else if( isset( $social_post->videos->low_resolution ) ) {
                $data['video'] = $social_post->videos->low_resolution->url; 
            }
        }

        $data['image'] = get_instagram_post_photo( $social_post ); 

        return $data; 
    }
}