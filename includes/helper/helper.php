<?php 

if( !defined( 'ABSPATH' ) ) exit; 

if( !function_exists( 'wpsf_load_template' ) ) {
    function wpsf_get_template( $template_name, $slug ) {
        static $plugin_path; 
        if(!$plugin_path) {
            $plugin_path = WP_Social_Feed()->plugin_path;
        }

        // caching for optimization
        static $template_files; 
        if( !$template_files ) {
            $template_files = array(); 
        }

        $template_names = array(
            'generic' => $template_name . '.php', 
            'specific' => $template_name . '-' . $slug . '.php',
        ); 

        $template_key = $slug ? $template_name . '-' . $slug : $template_name; 

        if( isset( $template_files[$template_key] ) ) {
            return $template_files[$template_key]; 
        }

        $theme_templates = array(
            TEMPLATE_THEME_DIR . '/' . $template_names['generic'], 
            TEMPLATE_THEME_DIR . '/' . $template_names['specific']
        ); 

        $template = locate_template( $theme_templates ); 
        if( $template == '' ) {
            $generic = $plugin_path . 'templates/' . $template_names['generic'];
            $specific = $plugin_path . 'templates/' . $template_names['specific'];
            if( file_exists( $specific ) ) {
                $template = $specific; 
            } else if( file_exists( $generic ) ) {
                $template = $generic;
            } 
        }

        if( $template ) {
            $template_files[$template_key] = $template; 
        } else {
            $template = $plugin_path . 'templates/default.php'; //falback - so returned value can always be included/required
        }

        return $template; 
    }
}

if( !function_exists( 'wpsf_esc_html_attrs' ) ) {
    /**
    * outputs an array of attr => value to a html element format
    */
    function wpsf_esc_html_attrs( $args ) {
        // is string?
        if( is_string($args) ) {
            
            $args = trim( $args );
            return esc_attr( $args );
            
        }        
        
        // validate
        if( empty($args) ) {            
            return '';            
        }        
        
        // vars
        $e = array();        
        
        // loop through and render
        foreach( $args as $k => $v ) {            
            // object
            if( is_array($v) || is_object($v) ) {                
                $v = json_encode($v);            
            // boolean	
            } elseif( is_bool($v) && false === strpos( $k, 'data-' ) ) {         //output data- attributes normally       
                $v = $v ? 1 : 0;  
                if( $v ) {
                    $e[] = $k . '="' . esc_attr( $k ) . '"'; //disabled="disabled" style for legacy
                } 
                continue;        
            // string
            } elseif( is_bool( $v ) ) {
                $v = $v ? 'true' : 'false';  
            } elseif( is_string($v) ) {                
                $v = trim($v);                
            }  
            if( !empty( $v ) ) {
                // append
                $e[] = $k . '="' . esc_attr( $v ) . '"';
            }          
            
        }        
        
        // echo
        return implode(' ', $e);
    }
}

if( !function_exists( 'wpsf_whitelist_array' ) ) {
    function wpsf_whitelist_array( $arr, $whitelist ) {
        return array_intersect( $arr, $whitelist ); 
    }
}
    
if( !function_exists( 'wpsf_maybe_get' ) ) {
    function wpsf_maybe_get( $key, $obj, $default = null, $check_callback = null ) {
        if(isset( $obj[$key] ) ) {
            return $obj[$key]; 
        }
        return $default; 
    }
}

if( !function_exists( 'wpsf_maybe_get_depp' ) ) {
    function wpsf_maybe_get_deep( $key, $obj, $default = null, $check_callback = null ) {
        $keys = (array) $key; 
        $result = $obj; 
        foreach( $keys as $k ) {
            $result = wpsf_maybe_get( $k, $result, $default ); 
        }

        return $result; 
    }
}