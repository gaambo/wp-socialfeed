<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

final class WP_Social_Feed_Feed {

    protected $feed; 
    protected $feed_object; 
    protected $id; 

	public function __construct( $feed, $args ) {
        $this->feed = $feed; 
        $this->id = uniqid($this->feed); 

        $plugin = WP_Social_Feed(); 

        if(!array_key_exists( $feed, $plugin->social_feeds ) ) {
            throw new Exception( 'The specified feed "' . $feed . '" is not registered/does not exist.' );            
        }

        if( !class_exists( $plugin->social_feeds[$feed] ) ) {
            throw new Exception( 'The specified feed "' . $feed . '" is not a class.' );
        }

        $this->feed_object = new $plugin->social_feeds[$feed]( $args ); 

        if( !$this->feed_object ) {
            throw new Exception( 'could not instantiate feed' ); 
        }

        $plugin->feeds[$this->id] = $this; 
    }

    public function get_feed( $args ) {
        //HANDLE ARGS
        $defaults = array(
        );

        $args = wp_parse_args( $args, $defaults );

        //GET POSTS
        //try gettings posts from cache
        $data = wp_cache_get( $this->feed, 'wpsf-feed-data' ); 
        if( !$data || empty( $data ) ) {
            $data = $this->feed_object->get_posts( $args ); 
            wp_cache_set( $this->feed, $data, 'wpsf-feed-data', $this->get_cache_expire_setting() ); 
        }

        $output = ''; 

        if( $data ) {
            //DISPLAY POSTS 
            ob_start(); 
            $this->display_feed( $data, $args ); 
            $output = ob_get_clean(); 
        }
        

        return $output; 
    }

    public function get_feed_object() {
        return $this->feed_object; 
    }

    protected function display_feed( $data, $args ) {
        //setting up globals
        global $social_posts; 
        $social_posts[$this->id]['data'] = $data; 
        $social_posts[$this->id]['args'] = $args; 
        global $feed_id; 
        $feed_id = $this->id; 

        //must call the_feed()
        include wpsf_get_template( 'feed' , $this->feed );
    }

    protected function get_cache_expire_setting( ) {
        $settings = WP_Social_Feed()->settings; 

        $cache_expire = $settings->get_value( 'cache_expire', false, $this->feed ); 
        if( !$cache_expire ) {
            $cache_expire = $settings->get_value( 'cache_expire', 0, 'general' ); 
        }

        return $cache_expire; 
    }
}

if( !function_exists( 'the_feed' ) ) {
    function the_feed() {
        global $social_posts; 
        global $feed_id;
        $plugin = WP_Social_Feed(); 
        $feed = $plugin->feeds[$feed_id];
        if( isset( $social_posts[$feed_id]['data']['posts'] ) && $social_posts[$feed_id]['data']['posts'] ) {
            foreach( $social_posts[$feed_id]['data']['posts'] as $social_post ) {
                echo $feed->get_feed_object()->display_post( $social_post, $social_posts[$feed_id]['args'] ); 
            }
        }
        
    }
}