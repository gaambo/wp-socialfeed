# WP Socialfeed

a social feed plugin for wordpress - for developers. 
ships with no styling. default templates are very basic and should be overwritten (they are just examples).

## Installation 
* copy folder to wp-content/plugins
* run `composer install` to install composer dependencies
* (no npm dependencies atm)

## Usage
as shortcode in page/post/content `[socialfeed feed="%feed% %attrs%]`

the feed attribute must be one of the registered feeds. 

this plugins ships with the following standard feeds
* facebook
* instagram

## Feeds

### Facebook

uses the facebook php sdk so working with this should be easier (and more standardized)

attributes: 

* page_id - a page id (id or string) 

### Instagram

TODO 
still in development - needs a access token because public feed api is deprecated/prohibited in instagram graph api 

## Templates
themes can overwrite templates in a subdirectory named `wpsf-templates` in their theme. 
the name/path of the subdirectory to look for in themes can be changed with the `wpsf/template_theme_dir` filter 

the following template slugs are used by default by this plugin: 

* default.php (all fallback) 
* feed-default.php (default feed) 
* feed-{$feed}.php (e.g. feed-facebook.php for the registered feed "facebook")
* post-default.php (default post for any feed) 
* post-{$feed}.php (e.g. post-facebook.php for a post of the registered feed "facebook")

registered feeds may call other templates as well (e.g. post-facebook-video.php). this must be checked with the registered feeds


## Developers 

other feeds can be registered in the `wpsf/feeds` filter and must implement the `WP_Social_Feed_Feed_Interface` interface or extend the `WP_Social_Feed_Feed_Abstract` class. 

